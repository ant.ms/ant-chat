<?php
    if ($_GET) {
        $username = $_GET['username'];
        $url = $_GET['url'];
        $content = $_GET['content'];
    } else {
        $username = $argv[1];
        $url = $argv[2];
        $content = $argv[3];
    }

    $fp = fopen("_messages.txt", 'a');
    fwrite($fp, date("H:i"));
    fwrite($fp, ';');
    fwrite($fp, $username);
    fwrite($fp, ';');
    fwrite($fp, $url);
    fwrite($fp, ';');
    fwrite($fp, "<img class=");
    fwrite($fp, "\"sentImage\"");
    fwrite($fp, "src=\"");
    fwrite($fp, $content);
    fwrite($fp, "\">");
    fwrite($fp, "\r\n");
    fclose($fp);
?>