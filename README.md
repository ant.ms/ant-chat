This project has been moved to [git.ant.lgbt/experiments/ant-chat](https://git.ant.lgbt/experiments/ant-chat)

![](https://stored.ant.lgbt/img/antChat.png)

The Ant Chat is one of the simplest and easiest to use chat applications out there. It consists of pure HTML, CSS, JS and PHP, in order to allow you to get out of the way and enjoy your conversations.

