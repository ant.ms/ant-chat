readPHP();
updateUsername();
updateTheme();


if (getCookie("username") == null || getCookie("username") == "") {
    changeUsername();
}

function changeUsername() {
    changeUsernameName();
    changeUsernameUrl();
    updateUsername();
}

function toggleSettingsView() {
    if (document.getElementById("settings").style.visibility == 'hidden') {
        document.getElementById("settings").style.visibility = 'visible';
        document.getElementById("settingsContent").style.left = '0px';
        closePopup()
    } else {
        document.getElementById("settings").style.visibility = 'hidden';
        document.getElementById("settingsContent").style.left = '300px';
    }
}

function changeUsernameName() {
    document.cookie = "username=" + prompt("Please enter your name", "Anonymous");
}

function changeUsernameUrl() {
    document.cookie = "url=" + prompt("Please your profile image URL", "https://duckduckgo.com/i/99f9773c.png");
}

function updateUsername() {
    document.getElementById("userUsername").innerHTML = "<a onclick=\"changeUsername()\">" + getCookie("username") + "</a>";
    document.getElementById("userIcon").src = getCookie("url");
}

function logout() {
    document.cookie = "username=";
    document.cookie = "url=";
    document.cookie = "style=";
    alert("Successfully logged out");
    window.close();
    location.reload();
}

/* https://stackoverflow.com/questions/10730362/get-cookie-by-name */
function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
  }

function writePHP(event = null) {
    if (event == null || event.keyCode == 13 || event.which == 13){
        var username = getCookie("username");
        var url = getCookie("url");
        var content = document.getElementById("inputBox").value;

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.responseText);
            }
        };
        xmlhttp.open("GET", "writeMessage.php?username=" + username + "&url=" + url + "&content=" + content, true);
        xmlhttp.send();
        document.getElementById("inputBox").value = "";
    }
}

function writeIMAGE() {
    var username = getCookie("username");
    var url = getCookie("url");
    var content = document.getElementById("inputBox").value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
        }
    };
    xmlhttp.open("GET", "writeMessageImage.php?username=" + username + "&url=" + url + "&content=" + content, true);
    xmlhttp.send();
    document.getElementById("inputBox").value = "";
}

function readPHP() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("messages").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("GET", "readChat.php", true);
    xmlhttp.send();
}
setInterval(() => {
    readPHP();
}, 200);

function toggleTheme() {
    if (getCookie("style") == "dark") {
        document.cookie = "style=light";
    } else {
        document.cookie = "style=dark";
    }
    updateTheme();
}

function updateTheme() {
    var newStyleSheet=document.createElement('link');
    newStyleSheet.rel='stylesheet';
    if (getCookie("style") == "dark") {
        newStyleSheet.href='style-dark.css';
    } else {
        newStyleSheet.href='style-light.css';
    }
    document.getElementsByTagName("head")[0].appendChild(newStyleSheet);
}

function about() {
    toggleSettingsView()
    document.getElementById("aboutPopup").style.visibility = 'visible';
}

function closePopup() {
    document.getElementById("aboutPopup").style.visibility = 'hidden';
}